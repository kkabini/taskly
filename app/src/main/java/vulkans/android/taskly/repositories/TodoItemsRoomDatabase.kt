package vulkans.android.taskly.repositories

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = [TodoItem::class], version = 1)
abstract class TodoItemsRoomDatabase : RoomDatabase() {

    abstract fun todoDao(): TodoItemDao

    companion object {

        @Volatile
        private var INSTANCE: TodoItemsRoomDatabase? = null

        fun getInstance(context: Context): TodoItemsRoomDatabase? {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        TodoItemsRoomDatabase::class.java,
                        "todo_store").build()
                INSTANCE = instance
                return instance
            }
        }
    }
}