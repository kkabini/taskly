package vulkans.android.taskly.presentation

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.todo_listitem.view.*
import vulkans.android.taskly.R
import vulkans.android.taskly.repositories.TodoItem
import vulkans.android.taskly.presentation.TodoItemsViewModel.*


class TodoItemsAdapter(private var todoItems: List<TodoItem> = ArrayList()) : RecyclerView.Adapter<TodoItemsAdapter.TodoItemViewHolder>() {

    lateinit var onTodoItemsEventCallback: OnTodoItemsEventCallback

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return TodoItemViewHolder(inflater.inflate(R.layout.todo_listitem, parent, false))
    }

    override fun onBindViewHolder(holder: TodoItemViewHolder, position: Int) {
        holder.onBind(todoItems[position])
    }

    override fun getItemCount(): Int = todoItems.size

    override fun getItemId(position: Int): Long {
        return todoItems[position].id.toLong()
    }

    fun refreshAdapter(newTodoItems: List<TodoItem>) {
        val mutableTodoItems = todoItems as ArrayList
        mutableTodoItems.clear()
        mutableTodoItems.addAll(newTodoItems)
        notifyDataSetChanged()
    }

    inner class TodoItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun onBind(todoItem: TodoItem) {
            view.headingTextView.text = todoItem.taskName

            if (!todoItem.notes.isNullOrEmpty()) {
                if (view.taskNotesTextView.visibility == View.GONE) {
                    view.taskNotesTextView.visibility = View.VISIBLE
                }
                view.taskNotesTextView.text = todoItem.notes
            } else {
                if (view.taskNotesTextView.visibility == View.VISIBLE) {
                    view.taskNotesTextView.visibility = View.GONE
                }
            }

            when (todoItem.status) {
                TaskStatus.PENDING.key -> view.taskCheckBox.isChecked = false
                TaskStatus.COMPLETED.key -> view.taskCheckBox.isChecked = true
            }

            view.taskCheckBox.setOnCheckedChangeListener { buttonView, isChecked ->
                val todoItem = todoItems[adapterPosition]
                if (isChecked) {
                    todoItem.status = TaskStatus.COMPLETED.key
                } else {
                    todoItem.status = TaskStatus.PENDING.key
                }
                onTodoItemsEventCallback.onTodoItemMarkedAsComplete(todoItem)
            }

            view.setOnClickListener {
                onTodoItemsEventCallback.onTodoItemSelected(todoItems[adapterPosition])
            }
        }
    }

    interface OnTodoItemsEventCallback {
        fun onTodoItemMarkedAsComplete(todoItem: TodoItem)
        fun onTodoItemSelected(todoItem: TodoItem)
    }

}