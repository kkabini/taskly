package vulkans.android.taskly.repositories

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*

@Dao
interface TodoItemDao {

    @Insert
    fun insertTodoItem(todoItem: TodoItem)

    @Delete
    fun deleteTodoItem(todoItem: TodoItem)

    @Update
    fun updateTodoItem(todoItem: TodoItem)

    @Query("SELECT * FROM todo_items")
    fun fetchAllTodoItems(): LiveData<List<TodoItem>>?

    @Query("SELECT * FROM todo_items where status = :status")
    fun fetchTodoItemsByStatus(status: String): LiveData<List<TodoItem>>?

    @Query("SELECT * FROM todo_items where priority = :priority")
    fun fetchTodoItemsByPriority(priority: String): LiveData<List<TodoItem>>?

    @Query("SELECT * FROM todo_items where category = :category")
    fun fetchTodoItemsByCategory(category: String): LiveData<List<TodoItem>>?

    @Query("SELECT COUNT(id) as totalTodoItems, (SELECT COUNT(id) FROM todo_items where status = 'completed') AS completedTodoItemsCount FROM todo_items")
    fun fetchCompletedTodoItemsCount(): LiveData<TodoItemCounter>?
}