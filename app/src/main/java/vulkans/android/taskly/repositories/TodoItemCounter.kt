package vulkans.android.taskly.repositories

data class TodoItemCounter (var totalTodoItems:Int,
                            var completedTodoItemsCount:Int)