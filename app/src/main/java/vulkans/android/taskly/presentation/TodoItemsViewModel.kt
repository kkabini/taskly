package vulkans.android.taskly.presentation

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import vulkans.android.taskly.repositories.TodoItem
import vulkans.android.taskly.repositories.TodoItemCounter
import vulkans.android.taskly.repositories.TodoItemsRepository
import vulkans.android.taskly.repositories.TodoItemsRepositoryImpl

class TodoItemsViewModel(application: Application) : AndroidViewModel(application) {
    private val todoItemsRepository:TodoItemsRepository

    val commandLiveData:MutableLiveData<String>
    val todoItemsLiveData:LiveData<List<TodoItem>>
    val progressLiveData:LiveData<TodoItemCounter>?

    init {
        todoItemsRepository = TodoItemsRepositoryImpl(application)
        commandLiveData =  MutableLiveData()
        todoItemsLiveData = Transformations.switchMap(commandLiveData){ keyword ->
            when(keyword){
                TaskStatus.PENDING.key, TaskStatus.COMPLETED.key -> todoItemsRepository.fetchTodoItemsByStatus(keyword)
                TaskPriority.HIGH.key, TaskPriority.MEDIUM.key, TaskPriority.LOW.key -> todoItemsRepository.fetchTodoItemsByPriority(keyword)
                TaskLabelType.BUSINESS.key, TaskLabelType.PERSONAL.key -> todoItemsRepository.fetchTodoItemsByCategory(keyword)
                 else -> todoItemsRepository.fetchAllTodoItems()
            }
        }
        progressLiveData = todoItemsRepository.fetchCompletedTodoItemsCount()
    }

    fun addTodoItem(todoItem: TodoItem) {
        todoItemsRepository.insertTodoItem(todoItem)
    }

    fun removeTodoItem(todoItem: TodoItem){
        todoItemsRepository.deleteTodoItem(todoItem)
    }

    fun updateTodoItem(todoItem: TodoItem){
        todoItemsRepository.updateTodoItem(todoItem)
    }

    fun loadTodoItems(keyword: String){
        commandLiveData.value = keyword
    }

    enum class TaskStatus(val key:String){
        PENDING("pending"),
        COMPLETED("completed")
    }

    enum class TaskPriority (val key:String){
        HIGH("high"),
        MEDIUM("medium"),
        LOW("low")
    }

    enum class TaskLabelType(val key: String){
        BUSINESS("business"),
        PERSONAL("personal")
    }

    fun toTitleCase(value: String?): String? {
        val stringBuilder = StringBuilder()
        return value?.let {
            when {
                it.isNotEmpty() -> {
                    stringBuilder.append(it[0].toUpperCase())
                    if (it.length > 1) {
                        stringBuilder.append(it.substring(1))
                    }
                    stringBuilder.toString()
                }
                else ->  ""
            }
        }
    }
}
