package vulkans.android.taskly.presentation

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import vulkans.android.taskly.R

class SplashActivity : AppCompatActivity() {
    private var handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)
    }

    override fun onResume() {
        super.onResume()
        val splashDelay = 1000L
        handler.postDelayed({
            startActivity(Intent(this, TodoItemsActivity::class.java))
            finish()
        }, splashDelay)
    }
}
