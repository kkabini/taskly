package vulkans.android.taskly.presentation

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.todo_items_fragment.*
import vulkans.android.taskly.R
import vulkans.android.taskly.repositories.TodoItem
import vulkans.android.taskly.presentation.TodoItemsViewModel.*

class TodoItemsFragment : Fragment() {
    private lateinit var todoItemsViewModel: TodoItemsViewModel
    private val todoItemsAdapter by lazy { TodoItemsAdapter() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.todo_items_fragment, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is TodoItemsActivity) {
            todoItemsViewModel = ViewModelProviders.of(context).get(TodoItemsViewModel::class.java)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let { homeActivity ->
            if (homeActivity is TodoItemsActivity) {
                homeActivity.setDrawerState(true)
            }

            val inputMethodManager = homeActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            val navController = findNavController()
            prepareRecyclerView(homeActivity)
            attachObservers(homeActivity)
            attachEventCallbackHandlers(navController, inputMethodManager)
            reloadTodoItems()
        }

    }

    private fun prepareRecyclerView(homeActivity: FragmentActivity) {
        val linearLayoutManager = LinearLayoutManager(homeActivity)
        val dividerItemDecoration = DividerItemDecoration(homeActivity, linearLayoutManager.orientation)
        todoItemsRecyclerView.layoutManager = linearLayoutManager
        todoItemsRecyclerView.addItemDecoration(dividerItemDecoration)
        todoItemsRecyclerView.setHasFixedSize(true)
        todoItemsRecyclerView.adapter = todoItemsAdapter
    }

    private fun toggleViews() {
        val todoItems = todoItemsViewModel.todoItemsLiveData.value
        if (todoItems == null || todoItems.isEmpty()) {
            unhideEmptyView()
        } else {
            unhideRecyclerView()
        }
    }

    private fun unhideRecyclerView() {
        if (emptyTextView.visibility == View.VISIBLE) {
            emptyTextView.visibility = View.GONE
        }

        if (todoItemsRecyclerView.visibility == View.GONE) {
            todoItemsRecyclerView.visibility = View.VISIBLE
        }
    }

    private fun unhideEmptyView() {
        if (todoItemsRecyclerView.visibility == View.VISIBLE) {
            todoItemsRecyclerView.visibility = View.GONE
        }

        if (emptyTextView.visibility == View.GONE) {
            emptyTextView.visibility = View.VISIBLE
        }
    }

    private fun attachObservers(homeActivity: FragmentActivity) {

        todoItemsViewModel.commandLiveData.observe(homeActivity, Observer {
            it?.let {keyword ->
                when(keyword){
                    TaskStatus.COMPLETED.key, "all", TaskLabelType.PERSONAL.key, TaskLabelType.BUSINESS.key -> {
                        if(priorityTitleTextView.visibility == View.VISIBLE){
                            priorityTitleTextView.visibility = View.GONE
                        }
                        todoTitleTextView.text = todoItemsViewModel.toTitleCase(keyword)
                    }
                    TaskPriority.LOW.key, TaskPriority.MEDIUM.key,TaskPriority.HIGH.key -> {
                        todoItemsViewModel.toTitleCase(getString(R.string.todo_list_title))
                        if(priorityTitleTextView.visibility == View.GONE){
                            priorityTitleTextView.visibility = View.VISIBLE
                        }
                        priorityTitleTextView.text = todoItemsViewModel.toTitleCase(keyword)
                    }
                    else ->  {
                        if(priorityTitleTextView.visibility == View.VISIBLE){
                            priorityTitleTextView.visibility = View.GONE
                        }
                        todoTitleTextView.text = todoItemsViewModel.toTitleCase(getString(R.string.todo_list_title))
                    }
                }
            }
        })
        todoItemsViewModel.progressLiveData?.observe(homeActivity, Observer {
            it?.let { todoItemCounter ->
                val progress = ((todoItemCounter.completedTodoItemsCount.toFloat() / todoItemCounter.totalTodoItems.toFloat()) * 100F).toInt()
                todoProgressBar?.progress = progress
                todoProgressBarPercentageTextView?.text = getString(R.string.progress_bar_percentage, progress)
            }

        })

        todoItemsViewModel.todoItemsLiveData.observe(homeActivity, Observer {
            it?.let { todoItems ->
                toggleViews()
                todoItemsAdapter.refreshAdapter(todoItems)
            }
        })
    }

    private fun attachEventCallbackHandlers(navController: NavController, inputMethodManager: InputMethodManager) {
        todoItemsAdapter.onTodoItemsEventCallback = object : TodoItemsAdapter.OnTodoItemsEventCallback {
            override fun onTodoItemSelected(todoItem: TodoItem) {
                val bundle = Bundle()
                bundle.putParcelable(TodoItemUpdateFragment::class.simpleName, todoItem)
                navController.navigate(R.id.action_TodoItemsFragment_to_TodoItemUpdateFragment, bundle)
            }

            override fun onTodoItemMarkedAsComplete(todoItem: TodoItem) {
                todoItemsViewModel.updateTodoItem(todoItem)
            }
        }

        taskAddButton.setOnClickListener {
            if (taskEntryTextView.text.isNotEmpty()) {
                val todoItem = TodoItem(taskName = taskEntryTextView.text.toString())
                todoItemsViewModel.addTodoItem(todoItem)
                taskEntryTextView.setText("")
                inputMethodManager.hideSoftInputFromWindow(taskEntryTextView.windowToken, 0)
            }
        }
    }

    private fun reloadTodoItems() {
        val keyword = todoItemsViewModel.commandLiveData.value
        if (keyword == null) {
            todoItemsViewModel.loadTodoItems(TodoItemsViewModel.TaskStatus.PENDING.key)
        } else {
            todoItemsViewModel.loadTodoItems(keyword)
        }
    }

}
