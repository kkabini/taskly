package vulkans.android.taskly.repositories

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity(tableName = "todo_items")
data class TodoItem(@PrimaryKey(autoGenerate = true) var id:Int = 0,
                    var taskName:String="",
                    var notes:String="",
                    var status: String="pending",
                    var priority: String = "none",
                    var category:String="personal",
                    var dateTime: Long?=null) :Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Long::class.java.classLoader) as? Long) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(taskName)
        parcel.writeString(notes)
        parcel.writeString(status)
        parcel.writeString(priority)
        parcel.writeString(category)
        parcel.writeValue(dateTime)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TodoItem> {
        override fun createFromParcel(parcel: Parcel): TodoItem {
            return TodoItem(parcel)
        }

        override fun newArray(size: Int): Array<TodoItem?> {
            return arrayOfNulls(size)
        }
    }
}