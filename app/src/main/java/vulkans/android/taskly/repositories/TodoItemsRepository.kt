package vulkans.android.taskly.repositories

import android.arch.lifecycle.LiveData

interface TodoItemsRepository {
    fun insertTodoItem(todoItem: TodoItem)
    fun updateTodoItem(todoItem: TodoItem)
    fun deleteTodoItem(todoItem: TodoItem)
    fun fetchAllTodoItems(): LiveData<List<TodoItem>>?
    fun fetchTodoItemsByStatus(status: String):LiveData<List<TodoItem>>?
    fun fetchTodoItemsByPriority(priority: String): LiveData<List<TodoItem>>?
    fun fetchTodoItemsByCategory(category: String): LiveData<List<TodoItem>>?
    fun fetchCompletedTodoItemsCount(): LiveData<TodoItemCounter>?
}