package vulkans.android.taskly.presentation


import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.RadioButton
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.labels_dialog.*
import kotlinx.android.synthetic.main.priority_dialog.*
import kotlinx.android.synthetic.main.todo_item_detailed_view.view.*
import kotlinx.android.synthetic.main.todo_item_update_fragment.*
import vulkans.android.taskly.R
import vulkans.android.taskly.repositories.TodoItem
import java.text.SimpleDateFormat
import java.util.*
import vulkans.android.taskly.presentation.TodoItemsViewModel.TaskStatus.*;

class TodoItemUpdateFragment : Fragment() {
    private lateinit var todoItemsViewModel: TodoItemsViewModel
    private var priorityDialog: AlertDialog? = null
    private var labelDialog: AlertDialog? = null
    private val dateFormat = SimpleDateFormat("dd MMM, YYYY @ HH:mm", Locale.getDefault())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is TodoItemsActivity) {
            todoItemsViewModel = ViewModelProviders.of(context).get(TodoItemsViewModel::class.java)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.todo_item_update_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity.let {
            if(it is TodoItemsActivity){
                it.setDrawerState(false)
            }
        }
        arguments?.let {
            val todoItem = it[TodoItemUpdateFragment::class.simpleName]
            if (todoItem is TodoItem) {
                bindDataToViews(todoItem)
                attachViewCallbackHandlers(todoItem)
            }
        }
    }

    private fun bindDataToViews(todoItem: TodoItem) {

        if(todoItem.taskName.isEmpty()){
            taskNameDetailedItemView.untintDrawable()
        }else{
            taskNameDetailedItemView.setContent(todoItem.taskName)
            taskNameDetailedItemView.tintDrawable()
        }

        if(todoItem.notes.isEmpty()){
            notesDetailedItemView.untintDrawable()
        }else {
            notesDetailedItemView.setContent(todoItem.notes)
            notesDetailedItemView.tintDrawable()
        }

        val todoDateTime = todoItem.dateTime
        if(todoDateTime == null){
            dueDateDetailedItemView.untintDrawable()
        } else {
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = todoDateTime
            dueDateDetailedItemView.setContent(dateFormat.format(calendar.time))
            dueDateDetailedItemView.tintDrawable()
        }


        if(todoItem.priority.isEmpty() || todoItem.priority.equals( "None", true)){
            priorityDetailedItemView.untintDrawable()
        } else{
            priorityDetailedItemView.setContent(todoItem.priority)
            priorityDetailedItemView.tintDrawable()
        }

        if(todoItem.category.isEmpty()){
            labelsDetailedItemView.untintDrawable()
        } else {
            labelsDetailedItemView.setContent(todoItem.category)
            labelsDetailedItemView.tintDrawable()
        }

        when (todoItem.status) {
            TodoItemsViewModel.TaskStatus.COMPLETED.key -> statusDetailedItemView.setLeftIcon(R.drawable.ic_status_checked)
        }
    }

    private fun attachViewCallbackHandlers(todoItem: TodoItem) {

        dueDateDetailedItemView.setOnClickListener {
            showDateTimeChooser(todoItem)
        }

        priorityDetailedItemView.setOnClickListener {
            showPriorityChooser(todoItem)
        }


        labelsDetailedItemView.setOnClickListener {
            showLabelChooser(todoItem)
        }

        statusDetailedItemView.setOnClickListener {
            when (statusDetailedItemView.drawableResourceId) {
                R.drawable.ic_status_unchecked -> {
                    todoItem.status = COMPLETED.key
                    statusDetailedItemView.setLeftIcon(R.drawable.ic_status_checked)
                }
                R.drawable.ic_status_checked -> {
                    todoItem.status = PENDING.key
                    statusDetailedItemView.setLeftIcon(R.drawable.ic_status_unchecked)
                }
            }
        }

        activity?.let { context ->
            val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            todoItemsFloatingActionButton.setOnClickListener {
                todoItem.taskName = taskNameDetailedItemView.subHeadingTextView.text.toString()
                todoItem.notes = notesDetailedItemView.subHeadingTextView.text.toString()

                todoItemsViewModel.updateTodoItem(todoItem)
                if (taskNameDetailedItemView.hasFocus()) {
                    inputMethodManager.hideSoftInputFromWindow(taskNameDetailedItemView.windowToken, 0)
                } else if (notesDetailedItemView.hasFocus()) {
                    inputMethodManager.hideSoftInputFromWindow(notesDetailedItemView.windowToken, 0)
                }
                findNavController().popBackStack()
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.update_fragment_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.delete -> {
                arguments?.let {
                    val todoItem = it[TodoItemUpdateFragment::class.simpleName]
                    if (todoItem is TodoItem) {
                        todoItemsViewModel.removeTodoItem(todoItem)
                        findNavController().popBackStack()
                    }
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }



    private fun showPriorityChooser(todoItem: TodoItem) {
        activity?.let { context ->
            if (priorityDialog == null) {
                val dialogView = LayoutInflater.from(context).inflate(R.layout.priority_dialog, null)
                priorityDialog = AlertDialog.Builder(context)
                        .setTitle(R.string.task_priority)
                        .setView(dialogView)
                        .setNegativeButton(android.R.string.cancel, null)
                        .create()
            }
            priorityDialog?.show()
            priorityDialog?.priorityRadioButtonGroup?.setOnCheckedChangeListener { group, checkedId ->
                val checkedRadioButton = group.findViewById<RadioButton>(checkedId)
                val priority = checkedRadioButton.text.toString()
                todoItem.priority = priority.toLowerCase()
                if(priority.equals("None", true)){
                    priorityDetailedItemView.untintDrawable()
                } else{
                    priorityDetailedItemView.tintDrawable()
                }
                priorityDetailedItemView.setContent(priority)
                priorityDialog?.dismiss()
            }
        }

    }

    private fun showLabelChooser(todoItem: TodoItem) {
        activity?.let { context ->
            if (labelDialog == null) {
                val dialogView = LayoutInflater.from(context).inflate(R.layout.labels_dialog, null)
                labelDialog = AlertDialog.Builder(context)
                        .setTitle(R.string.task_label)
                        .setView(dialogView)
                        .setNegativeButton(android.R.string.cancel, null)
                        .create()
            }
            labelDialog?.show()

            labelDialog?.categoryRadioButtonGroup?.setOnCheckedChangeListener { group, checkedId ->
                val checkedRadioButton = group.findViewById<RadioButton>(checkedId)
                val label = checkedRadioButton.text.toString()
                todoItem.category = label.toLowerCase()
                labelsDetailedItemView.setContent(label)
                labelsDetailedItemView.tintDrawable()
                labelDialog?.dismiss()
            }
        }

    }

    private fun showDateTimeChooser(todoItem: TodoItem) {
        val selectedDate = Calendar.getInstance()
        val today = Calendar.getInstance()
        val day = today.get(Calendar.DAY_OF_MONTH)
        val month = today.get(Calendar.MONTH)
        val year = today.get(Calendar.YEAR)
        val hour = today.get(Calendar.HOUR_OF_DAY)
        val minute = today.get(Calendar.MINUTE)

        activity?.let { context ->
            DatePickerDialog(context, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                selectedDate.set(year, month, dayOfMonth)
                TimePickerDialog(context, TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    selectedDate.set(Calendar.HOUR_OF_DAY, hourOfDay)
                    selectedDate.set(Calendar.MINUTE, minute)
                    dueDateDetailedItemView.setContent(dateFormat.format(selectedDate.time))
                    todoItem.dateTime = selectedDate.timeInMillis
                    dueDateDetailedItemView.tintDrawable()
                }, hour, minute, false).show()
            }, year, month, day).show()
        }
    }
}
