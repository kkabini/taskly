package vulkans.android.taskly.repositories

import android.app.Application
import android.arch.lifecycle.LiveData
import android.os.AsyncTask

class TodoItemsRepositoryImpl(application: Application) : TodoItemsRepository {
    private val todoItemDao: TodoItemDao?
    private val todoRoomDatabase = TodoItemsRoomDatabase.getInstance(application)

    init { todoItemDao = todoRoomDatabase?.todoDao() }

    override fun insertTodoItem(todoItem: TodoItem) { InsertAsyncTask(todoItemDao).execute(todoItem) }

    override fun updateTodoItem(todoItem: TodoItem) { UpdateAsyncTask(todoItemDao).execute(todoItem) }

    override fun deleteTodoItem(todoItem: TodoItem) { DeleteAsyncTask(todoItemDao).execute(todoItem) }

    override fun fetchAllTodoItems(): LiveData<List<TodoItem>>? = todoItemDao?.fetchAllTodoItems()

    override fun fetchTodoItemsByStatus(status: String): LiveData<List<TodoItem>>? = todoItemDao?.fetchTodoItemsByStatus(status)

    override fun fetchTodoItemsByPriority(priority: String): LiveData<List<TodoItem>>? = todoItemDao?.fetchTodoItemsByPriority(priority)

    override fun fetchTodoItemsByCategory(category: String): LiveData<List<TodoItem>>? = todoItemDao?.fetchTodoItemsByCategory(category)

    override fun fetchCompletedTodoItemsCount(): LiveData<TodoItemCounter>?= todoItemDao?.fetchCompletedTodoItemsCount()

    inner class InsertAsyncTask(private val todoItemDao: TodoItemDao?) : AsyncTask<TodoItem, Void, Void?>() {
        override fun doInBackground(vararg params: TodoItem): Void? {
            todoItemDao?.insertTodoItem(params[0])
            return null
        }
    }

    inner class UpdateAsyncTask(private val todoItemDao: TodoItemDao?) : AsyncTask<TodoItem, Void, Void?>() {
        override fun doInBackground(vararg params: TodoItem): Void? {
            todoItemDao?.updateTodoItem(params[0])
            return null
        }
    }

    inner class DeleteAsyncTask(private val todoItemDao: TodoItemDao?) : AsyncTask<TodoItem, Void, Void?>() {
        override fun doInBackground(vararg params: TodoItem): Void? {
            todoItemDao?.deleteTodoItem(params[0])
            return null
        }
    }
}