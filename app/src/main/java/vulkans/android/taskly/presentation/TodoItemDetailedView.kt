package vulkans.android.taskly.presentation

import android.content.Context
import android.support.annotation.DrawableRes
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.text.InputType
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import kotlinx.android.synthetic.main.todo_item_detailed_view.view.*
import vulkans.android.taskly.R

class TodoItemDetailedView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var drawableResourceId: Int

    init {
        LayoutInflater.from(context).inflate(R.layout.todo_item_detailed_view, this)
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.TodoItemDetailedView)
        drawableResourceId = typedArray.getResourceId(R.styleable.TodoItemDetailedView_attr_image_icon, android.R.drawable.star_off)
        val headingContent = typedArray.getString(R.styleable.TodoItemDetailedView_attr_heading_content)
        val subHeadingContent = typedArray.getString(R.styleable.TodoItemDetailedView_attr_subheading_content)
        val subHeadingHint = typedArray.getString(R.styleable.TodoItemDetailedView_attr_subheading_hint)
        val subHeadingEditable = typedArray.getBoolean(R.styleable.TodoItemDetailedView_attr_subheading_editable, true)
        val showSubHeading = typedArray.getBoolean(R.styleable.TodoItemDetailedView_attr_show_subheading, true)
        typedArray.recycle()

        iconImageView.setImageResource(drawableResourceId)
        headingTextView.text = headingContent

        if (showSubHeading) {
            subHeadingTextView.setText(toTitleCase(subHeadingContent))
            subHeadingTextView.hint = subHeadingHint
            if (subHeadingEditable)
                subHeadingTextView.setRawInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES or InputType.TYPE_TEXT_FLAG_MULTI_LINE)
            else
                subHeadingTextView.setRawInputType(InputType.TYPE_NULL)
        } else {
            if (subHeadingTextView.visibility == View.VISIBLE) subHeadingTextView.visibility = View.GONE
        }
    }

    private fun toTitleCase(value: String?): String? {
        val stringBuilder = StringBuilder()
        return value?.let {
           when {
                it.isNotEmpty() -> {
                    stringBuilder.append(it[0].toUpperCase())
                    if (it.length > 1) {
                        stringBuilder.append(it.substring(1))
                    }
                    stringBuilder.toString()
                }
                else ->  ""
            }
        }
    }

    fun setContent(value:String){
        subHeadingTextView.setText(toTitleCase(value))
    }

    fun setLeftIcon(drawableResourceId:Int) {
        this.drawableResourceId = drawableResourceId
        iconImageView.setImageResource(drawableResourceId)
    }

    fun tintDrawable(){
        DrawableCompat.setTint(iconImageView.drawable, ContextCompat.getColor(context, R.color.colorPrimary))
    }

    fun untintDrawable(){
        DrawableCompat.setTint(iconImageView.drawable, ContextCompat.getColor(context, R.color.colorGrey500))
    }
}